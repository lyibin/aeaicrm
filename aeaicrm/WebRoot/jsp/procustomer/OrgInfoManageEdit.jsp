<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doCreatePhoneCallAction(){
	doSubmit({actionType:'doCreatePhoneCallAction'});
}
function doApporve(){
	doSubmit({actionType:'doApporve'});
}
function doRevokeApporve(){
	doSubmit({actionType:'doRevokeApporve'});
}
function doSaveAction(){
	if(validate()){
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
			if ("success" == responseText){
					window.location.href = "index?OrgInfoManageList";
				}else if("repeatName" == responseText){
						hideSplash();
						writeErrorMsg('客户名称重复，请检查！');
					}else if("repeatMail" == responseText){
						hideSplash();
						writeErrorMsg('邮箱名重复，请检查！');
					}else{
						hideSplash();
						writeErrorMsg('保存操作出错啦！');					
				}
			}
		})}
		return;
}
var orgIdBox;
function openProIdBox(){
	var handlerId = "LabelsTreeSelect"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择标签',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetName=ORG_LABELS_NAME'+'&targetId=ORG_LABELS';
	orgIdBox.sendRequest(url);
}
function openSalerBox(){
	var handlerId = "SalerListSelectList"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择人员',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&ORG_ID='+$('#ORG_ID').val()+'&targetId=ORG_SALESMAN';
	orgIdBox.sendRequest(url);
}
function assignedSale(){
	doSubmit({actionType:'assignedSale'});
}
function assignedSale(){
	$('#ORG_SALESMAN').val();
	doSubmit({actionType:'assignedSale'});
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
	<div class="newarticle1" onclick="changeSubTable('_base')">基本信息</div>
	<div class="newarticle1" onclick="changeSubTable('visit')">拜访记录</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;display:none;overflow:hidden;">
<%
if("_base".equals(pageBean.inputValue("currentSubTableId"))){
%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSaveAction()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <%if (!"insert".equals(pageBean.getOperaType())){%>
   	<aeai:previlege code="assign"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openSalerBox()"><input value="&nbsp;" type="button" class="assignImgBtn" id="assignImgBtn" title="分配" />分配</td></aeai:previlege>
   <%} %>
	
<%}%>
<aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="ORG_NAME" label="客户名称" name="ORG_NAME" type="text" value="<%=pageBean.inputValue("ORG_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>分类</th>
	<td colspan="3"><select id="ORG_CLASSIFICATION" label="分类" name="ORG_CLASSIFICATION" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_CLASSIFICATION")%></select>
</td>
</tr>
<tr>

</tr>
<tr>
	<th width="100" nowrap>客户性质</th>
	<td><select id="ORG_TYPE" label="客户性质" name="ORG_TYPE" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_TYPE")%></select>
</td>
	<th width="100" nowrap>来源渠道</th>
	<td colspan="3"><select id="ORG_SOURCES" label="来源渠道" name="ORG_SOURCES" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_SOURCES")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>联系人</th>
	<td><input id="ORG_LINKMAN_NAME" label="联系人" name="ORG_LINKMAN_NAME" type="text" value="<%=pageBean.inputValue("ORG_LINKMAN_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>邮箱</th>
	<td colspan="3"><input id="ORG_EMAIL" label="邮箱" name="ORG_EMAIL" type="text" value="<%=pageBean.inputValue("ORG_EMAIL")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>标签</th>
	<td><input id="ORG_LABELS_NAME" name="ORG_LABELS_NAME" type="text" value="<%=pageBean.inputValue("ORG_LABELS_NAME")%>" size="24" style="width: 302px" class="text" readonly="readonly" />
    <input id="ORG_LABELS" name="ORG_LABELS" type="hidden" value="<%=pageBean.inputValue("ORG_LABELS")%>" size="24" style="width: 302px" class="text"/><img id="orgIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()"/>
</td>
	<th width="100" nowrap>公司网站</th>
	<td colspan="3"><input id="ORG_WEBSITE" label="公司网站" name="ORG_WEBSITE" type="text" value="<%=pageBean.inputValue("ORG_WEBSITE")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>跟进人员</th>
	<td><input name="ORG_SALESMAN_NAME" type="text" class="text"	id="ORG_SALESMAN_NAME"	value="<%=pageBean.inputValue("ORG_SALESMAN_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="跟进人员" /> 
		<input id="ORG_SALESMAN" label="跟进人员"  name="ORG_SALESMAN" type="hidden"	value="<%=pageBean.inputValue("ORG_SALESMAN")%>"  size="24" class="text" />
</td>
	<th width="100" nowrap>状态</th>
	<td colspan="3"><%=pageBean.selectRadio("ORG_STATE")%>
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="ORG_CREATER_NAME" type="text" class="text"	id="ORG_CREATER_NAME"	value="<%=pageBean.inputValue("ORG_CREATER_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="创建人" /> 
		<input id="ORG_CREATER" label="创建人"  name="ORG_CREATER" type="hidden"	value="<%=pageBean.inputValue("ORG_CREATER")%>"  size="24" class="text" />
</td>
	<th width="100" nowrap>创建时间</th>
	<td colspan="3"><input id="ORG_CREATE_TIME" label="创建时间" name="ORG_CREATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_CREATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />	
</td>
</tr>
<tr>
	<th width="100" nowrap>更新时间</th>
	<td><input id="ORG_UPDATE_TIME" label="更新时间" name="ORG_UPDATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_UPDATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>计划再次拜访时间</th>
	<td colspan="3"><input id="ORG_VISIT_AGAIN_TIME" label="更新时间" name="ORG_VISIT_AGAIN_TIME" type="text" value="<%=pageBean.inputTime("ORG_VISIT_AGAIN_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系方式</th>
	<td><input id="ORG_CONTACT_WAY" label="联系方式" name="ORG_CONTACT_WAY" type="text" value="<%=pageBean.inputValue("ORG_CONTACT_WAY")%>" size="24" style="width:90%" class="text" />
</td>
	<th width="100" nowrap>地址</th>
	<td colspan="3"><input id="ORG_ADDRESS" label="地址" name="ORG_ADDRESS" type="text" value="<%=pageBean.inputValue("ORG_ADDRESS")%>" size="24" style="width:90%" class="text" />
</td>
</tr>
<tr>
</tr>
<tr>
<th width="100" nowrap>简要介绍</th>
	<td colspan="3"><textarea id="ORG_INTRODUCTION" label="简要介绍" name="ORG_INTRODUCTION" style="width:95%" cols="60" rows="5" maxlength="256" class="textarea"><%=pageBean.inputValue("ORG_INTRODUCTION")%></textarea>
</td>
</tr>
</table>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;display:none;overflow:hidden;">
<%
if("visit".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?ProcustVisitManageList&ORG_ID=<%=pageBean.inputValue("ORG_ID")%>" width="100%" height="570" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus({"id":"<%=pageBean.inputValue("currentLayerleId")%>"});
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
</form>
<script language="javascript">
$('#ORG_INTRODUCTION').inputlimiter({
	limit: 500,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("ORG_NAME");
// requiredValidator.add("ORG_CONTACT_WAY");
initDetailOpertionImage();
new BlankTrimer("ORG_NAME");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
