package com.agileai.crm.module.mytasks.handler;

import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;

public class TaskReviewEditHandler
        extends TreeAndContentManageEditHandler {
    public TaskReviewEditHandler() {
        super();
        this.serviceId = buildServiceId(TaskCycle8ContentManage.class);
        this.tabId = "TaskReview";
        this.columnIdField = "TC_ID";
        this.contentIdField = "TASK_REVIEW_ID";
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected TaskCycle8ContentManage getService() {
        return (TaskCycle8ContentManage) this.lookupService(this.getServiceId());
    }
}
