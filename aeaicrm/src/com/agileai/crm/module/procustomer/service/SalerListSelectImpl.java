package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.crm.cxmodule.SalerListSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class SalerListSelectImpl
        extends PickFillModelServiceImpl
        implements SalerListSelect {
    public SalerListSelectImpl() {
        super();
    }
    public List<DataRow> querySaleListRecords() {
		String statementId = sqlNameSpace+"."+"querySaleListRecords";
		DataParam param = new DataParam();
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
