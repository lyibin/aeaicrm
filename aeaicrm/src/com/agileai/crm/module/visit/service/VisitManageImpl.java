package com.agileai.crm.module.visit.service;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class VisitManageImpl
        extends StandardServiceImpl
        implements VisitManage {
    public VisitManageImpl() {
        super();
    }

	@Override
	public void updateFillInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateFillStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}

	public void updateConfirmStateInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateConfirmStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void createClueRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createCLueRecord";
		String clueId = KeyGenerator.instance().genKey();
   		param.put("OPP_ID",clueId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void createContRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createContRecord";
		String contId = KeyGenerator.instance().genKey();
   		param.put("CONT_ID",contId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public DataRow getProRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getProRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	
	public void updateVisitStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateVisitStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
}
