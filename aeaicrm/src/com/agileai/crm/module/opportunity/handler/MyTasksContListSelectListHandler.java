package com.agileai.crm.module.opportunity.handler;

import com.agileai.crm.cxmodule.ContListSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;

public class MyTasksContListSelectListHandler
        extends PickFillModelHandler {
    public MyTasksContListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(ContListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "CONT_NAME", "");
    }

    protected ContListSelect getService() {
        return (ContListSelect) this.lookupService(this.getServiceId());
    }
}
