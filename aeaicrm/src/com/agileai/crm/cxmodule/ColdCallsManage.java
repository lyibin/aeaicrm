package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface ColdCallsManage
        extends MasterSubService {
	DataRow getProCustRecord(DataParam param);
	void createProVisitRecord(DataParam param);
	void updateTaskStateRecord(DataParam param);
	void updateTaskClassRecord(DataParam param);
	DataRow getCustInfoRecord(DataParam param);
	void updateCustStateRecord(DataParam param);
	DataRow getTaskReviewRecord(DataParam param);
	void createProCustInfoRecord(DataParam param);
	void updateTaskOrgIdRecord(DataParam param);
	void updateTaskOrgStateRecord(DataParam param);
	DataRow getCustIdInfoRecord(DataParam param);
}
